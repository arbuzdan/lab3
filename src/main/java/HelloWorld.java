public class HelloWorld {
    private String name;
    private int age;
    private Greetings g;
    private Age a;

    public void setName(String name) {
        this.name = name;
    }

    public void setG(Greetings g) {
        this.g = g;
    }

    public void setA(Age a){
        this.a = a;
    }

    public void printHello() {
        System.out.println("Hello, " + name + "!" + "\n"
                + g.getGreetings() + "\n"
                + "Your age is " + a.getAge() + ", isnt it?:)");
    }
}